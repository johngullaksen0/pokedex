import React, { Component } from 'react';
import typeColor from './typeColors';

export default class PokeType extends Component {

    render() {
        var {type} = this.props;
        // if type is "" we don't want to render anything
        if (type == null) return "";
        
        // if type is not in map we return none type
        type = (typeColor.hasOwnProperty(type)) ? type : "none";

        // basic styling to achieve the type tag look
        const typeStyle = {
            background: typeColor[type].primary,
            border: '2px solid',
            borderColor: typeColor[type].secondary,
            borderRadius: 90,
            color: "white",
            textAlign: "center",
            fontSize: 16,
            width: 75,
            height: 25,
            marginRight: 10,
        }
        return (
            <div style={typeStyle}>
                {upper(type)}
            </div>
        );
    }
}

function upper(s) 
{
    return s.charAt(0).toUpperCase() + s.slice(1);
}
