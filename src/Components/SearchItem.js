import React, { Component } from 'react';
import PokePortrait from '../Components/PokePortrait';
import PokeType from '../Components/PokeType';
import { observer } from 'mobx-react';

const SearchItem = observer(class SearchItem extends Component {
    
    // list item of a pokemon in the result set
    render() {
        const {pokemon, onClickMethod} = this.props;
        const itemStyle = {
            display: "flex",
            alignItems: "center",
        }
        return(
            <div style={itemStyle} onClick={() => onClickMethod(pokemon)}>
                <PokePortrait pokemon={pokemon} height={50} width={50} />
                <h3 style={{marginRight: 20, marginLeft: 20}}>#{pokemon.pokedex_number} {upper(pokemon.name)}</h3>
                <PokeType type={pokemon.type1}/>
                <PokeType type={pokemon.type2}/>
            </div>
        );
    }
})
export default SearchItem;

function upper(s) 
{
    return s.charAt(0).toUpperCase() + s.slice(1);
}