import React, { Component } from 'react';
import { RadarChart } from 'react-vis';
import typeColor from './typeColors';

// The first data elements here are to simulate a 'spider' type of radar chart
// with the default radar char you can only get circular separators
const DATA = [
    {
        name: 'line250',
        hp: 250,
        attack: 250,
        defense: 250,
        sp_attack: 250,
        sp_defense: 250,
        speed: 250,
        fill: '#ffffff',
        stroke: '#cccccc'
    },
    {
        name: 'line200',
        hp: 200,
        attack: 200,
        defense: 200,
        sp_attack: 200,
        sp_defense: 200,
        speed: 200,
        fill: '#ffffff',
        stroke: '#cccccc'
    },
    {
        name: 'line150',
        hp: 150,
        attack: 150,
        defense: 150,
        sp_attack: 150,
        sp_defense: 150,
        speed: 150,
        fill: '#ffffff',
        stroke: '#cccccc'
    },
    {
        name: 'line100',
        hp: 100,
        attack: 100,
        defense: 100,
        sp_attack: 100,
        sp_defense: 100,
        speed: 100,
        fill: '#ffffff',
        stroke: '#cccccc'
    },
    {
        name: 'line50',
        hp: 50,
        attack: 50,
        defense: 50,
        sp_attack: 50,
        sp_defense: 50,
        speed: 50,
        fill: '#ffffff',
        stroke: '#cccccc'
    },
    {
        name: 'line1',
        hp: 1,
        attack: 1,
        defense: 1,
        sp_attack: 1,
        sp_defense: 1,
        speed: 1,
        fill: '#ffffff',
        stroke: '#cccccc'
    },
    {
        dummy: "item" // this is a dummy item to make sure the popping works properly
    }
];

export default class PokeStats extends Component {
    render() {
        const {pokemon} = this.props;
        // we want to add the stats for this pokemon to the data-set
        try {
            DATA.pop();
            DATA.push(
                {
                    name: pokemon.name,
                    hp: pokemon.hp,
                    attack: pokemon.attack,
                    defense: pokemon.defense,
                    sp_attack: pokemon.sp_attack,
                    sp_defense: pokemon.sp_defense,
                    speed: pokemon.speed,
                    fill: typeColor[pokemon.type1].primary,
                    stroke: typeColor[pokemon.type1].secondary
                }
            )
        } catch (e) {
            
        }

        // this is just passing the correct mapping and styling to the react-vis radar chart
        return (
            <RadarChart
                data={DATA}
                margin={{left: 40, right: 40, top: 40, bottom: 40}}
                tickFormat={t => {
                    return Math.round(t - t/50);
                }}
                domains={[
                    { name: 'HP', domain: [0, 255], getValue: d => d.hp },
                    { name: 'Atk', domain: [0, 255], getValue: d => d.attack },
                    { name: 'Def', domain: [0, 255], getValue: d => d.defense },
                    { name: 'Sp. atk', domain: [0, 255], getValue: d => d.sp_attack },
                    { name: 'Sp. def', domain: [0, 255], getValue: d => d.sp_defense },
                    { name: 'Speed', domain: [0, 255], getValue: d => d.speed }
                ]}
                width={this.props.width}
                height={this.props.height}
                style={{
                    polygons: {
                        strokeWidth: 1,
                        strokeOpacity: 0.8,
                        fillOpacity: 0.8
                    },
                    labels: {
                        fontSize: 16,
                        textAnchor: 'middle'
                    },
                    axes: {
                        line: {
                            fillOpacity: 0.8,
                            strokeWidth: 0.5,
                            strokeOpacity: 0.8
                        },
                        ticks: {
                            fillOpacity: 0,
                            strokeOpacity: 0
                        },
                        text: {
                            fontSize: 10,
                            color: "gray",
                        }
                    }
                }}
                colorRange={['transparent']}
                hideInnerMostValues={false}
                renderAxesOverPolygons={true}
            >
            </RadarChart>
        );
    }
}