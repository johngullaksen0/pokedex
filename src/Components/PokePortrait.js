import React, { Component } from 'react';
import typeColors from './typeColors.js';

export default class PokePortrait extends Component {
    render() {
        const {height, width, pokemon}  = this.props;
        const ringStyle = {
            background: typeColors[pokemon.type1].primary,
            borderRadius: "50%",
            border: `${Math.max(2, width/50)}px solid`,
            // use type2 secondary color if it type2 has a value else use type1 secondary color
            borderColor: typeColors[(pokemon.type2) ? pokemon.type2 : pokemon.type1].secondary,
            height: height,
            width: width,
            display: "flex",
            overflow: "hidden",
        }
        const imageStyle = {
            height: height,
            width: width,
        }
        var id = pokemon.pokedex_number;
        // this is to handle urls for mega forms, some alt. forms have less predicable urls so there might still be some missing images
        var variant = pokemon.name.split(/(-.+)/)[1]; // gets the part after the first dash of the name
        if(variant && (variant.startsWith("-mega") || variant.startsWith("-alola"))) { // mega and alola urls are predictable, we can use them
            id += variant; 
        }
        return (
            <div className="portraitContainer" style={ringStyle} >
                <img 
                    className="portrait"
                    style={imageStyle}
                    src={`https://pokeres.bastionbot.org/images/pokemon/${id}.png`}
                    alt={pokemon.name}
                />
            </div>
        )
    }
}
