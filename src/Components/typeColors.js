// this acts as an enum map to get primary and secondary colors for each type
const typeColors = {
    normal:     {primary: "#A8A878", secondary: "#6D6D4E"},
    fire:       {primary: "#F08030", secondary: "#9C531F"},
    fighting:   {primary: "#C03028", secondary: "#7D1F1A"},
    water:      {primary: "#6890F0", secondary: "#445E9C"},
    flying:     {primary: "#A890F0", secondary: "#6D5E9C"},
    grass:      {primary: "#78C850", secondary: "#4E8234"},
    poison:     {primary: "#A040A0", secondary: "#682A68"},
    electric:   {primary: "#F8D030", secondary: "#A1871F"},
    ground:     {primary: "#E0C068", secondary: "#927D44"},
    psychic:    {primary: "#F85888", secondary: "#A13959"},
    rock:       {primary: "#B8A038", secondary: "#786824"},
    ice:        {primary: "#98D8D8", secondary: "#638D8D"},
    bug:        {primary: "#A8B820", secondary: "#6D7815"},
    dragon:     {primary: "#7038F8", secondary: "#4924A1"},
    ghost:      {primary: "#705898", secondary: "#493963"},
    dark:       {primary: "#705848", secondary: "#49392F"},
    steel:      {primary: "#B8B8D0", secondary: "#787887"},
    fairy:      {primary: "#EE99AC", secondary: "#9B6470"},
    none:       {primary: "#68A090", secondary: "#44685E"},
}

export default typeColors