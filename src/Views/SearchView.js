import React, { Component } from 'react';
import SearchField from 'react-search-field';
import SearchItem from '../Components/SearchItem';
import ItemView from './ItemView';

import { observable, decorate } from 'mobx';
import { observer } from 'mobx-react';

import Modal from 'react-modal';
Modal.setAppElement("#root");

const SearchView = observer(class SearchView extends Component {

    // these are the observable values used with mobx, they are decorated with the decorate function below this class
    modalStatus = false;
    selectedPokemon = null; // this is the active pokemon in the modal
    results = [];
    selectedType = ""; // this is used for the type filter

    constructor() {
        super();

        // to make sure this refers to the expected class when passed as an onclick function
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.setSelectedPokemon = this.setSelectedPokemon.bind(this);
        this.setSelectedType = this.setSelectedType.bind(this);
    }



    openModal() {
        this.modalStatus = true;
    }

    closeModal() {
        this.modalStatus = false;
    }

    setSelectedPokemon(pokemon) {
        this.selectedPokemon = pokemon;
        this.openModal();
    }

    setSelectedType(event) {
        this.selectedType = event.target.value;
    }

    // when performing a search we check if it is a number or a name and pick the query method based on that
    search(input) {
        if (isNaN(input) || (input === "" && this.selectedType)) { // either type or name field has to have a value to send a request
            this.findPokemon(input, this.selectedType);
        } else {
            this.getPokemon(input);
        }
    }

    // function that gets a pokemon based of their pokedex number, will return one result
    getPokemon(dexNumber) {
        var query = `
            {
                getPokemon(dexNumber:${dexNumber}) {
                    pokedex_number
                    classification
                    name
                    height_m
                    weight_kg
                    type1
                    type2
                    ability1
                    ability2
                    ability3
                    generation
                    hp
                    attack
                    defense
                    sp_attack
                    sp_defense
                    speed
                }
            }
        `

        fetch('http://it2810-48.idi.ntnu.no:4000/graphql', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({ query: query })
        })
            .then(r => r.json())
            .then(data => {
                if (data) {
                    this.results = data.data.getPokemon
                }});

    }

    // function to find pokemon that match the search criteria
    findPokemon(name, type) {
        var typeExpr = (type === "") ? "" : `type: "${type}"`;
        var nameExpr = (name === "") ? "" : `name: "${name}"`;
        var query = `
        {
            findPokemon(${nameExpr} ${typeExpr}) {
                pokedex_number
                classification
                name
                height_m
                weight_kg
                type1
                type2
                ability1
                ability2
                ability3
                generation
                hp
                attack
                defense
                sp_attack
                sp_defense
                speed
            }
        }
    `
        fetch('http://it2810-48.idi.ntnu.no:4000/graphql', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                body: JSON.stringify({ query: query })
            })
                .then(r => r.json())
                .then(data => {
                    if (data) {
                        this.results = data.data.findPokemon
                    }});
    }

    render() {
        // type array is used for the drop down menu
        const types = ["", "normal", "fire", "fighting", "water", "flying", "grass", "poison", "electric", "ground", "psychic", "rock", "ice", "bug", "dragon", "ghost", "dark", "steel", "fairy"];
        return (
            <div id="root">

                <SearchField
                    placeholder='Search for a pokemon'
                    onSearchClick={input => this.search(input)}
                />
                Type filter: <select value={this.selectedType} onChange={this.setSelectedType}>
                    {types.map((t, i) => {return <option key={i} value={t}>{t}</option>})}
                </select> 
                {this.results.map((p, i) => { return <SearchItem pokemon={p} key={i} onClickMethod={this.setSelectedPokemon} /> })}
                <Modal
                    className="modal"
                    isOpen={this.modalStatus}
                    onRequestClose={this.closeModal}
                    contentLabel="Item view Modal"
                >
                    <button className="modalBtnClose" onClick={this.closeModal}>x</button>
                    <ItemView pokemon={this.selectedPokemon} />
                </Modal>
            </div>

        )
    }

})

// this is an alternative way to use mobx without decorators to make fields observable
decorate(SearchView, {
    selectedPokemon: observable,
    modalStatus: observable,
    results: observable,
    selectedType: observable,
})

export default SearchView

