import React, { Component } from 'react';
import PokePortrait from '../Components/PokePortrait';
import PokeStats from '../Components/PokeStats';
import PokeType from '../Components/PokeType';
import './ItemView.css'

export default class ItemView extends Component {
    getAbilityList(pokemon) {
        // reformatting of the ability fields into an array
        return [pokemon.ability1, pokemon.ability2, pokemon.ability3].filter(e => e != null).map(upper)
    }

    render() {
        const {pokemon} = this.props;
        if (!pokemon) {
            return <p>Pokemon not in dataset</p>;
        }        
        return (
            <div className="container">
                <PokePortrait className="portrait" pokemon={pokemon} height={250} width={250} />
                <div className="info">
                    <h1>#{pokemon.pokedex_number} {upper(pokemon.name)}</h1>
                    <div className="typing">
                        <PokeType type={pokemon.type1}/>
                        <PokeType type={pokemon.type2}/>
                    </div>
                    <p>Species: {pokemon.classification}</p>
                    <p>Generation: {pokemon.generation}</p>
                    <p>Height: {pokemon.height_m/10}m</p>
                    <p>Weight: {pokemon.weight_kg/10}kg</p>
                    <p>Abilities: {this.getAbilityList(pokemon).join(', ')}</p>

                </div>
                <div className="stats">
                    <PokeStats pokemon={pokemon} height={300} width={300} />
                </div>  
            </div>
        );
    }
}

function upper(s) 
{
    return s.charAt(0).toUpperCase() + s.slice(1);
}