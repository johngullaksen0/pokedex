import React, { Component } from 'react';
import './App.css';
import SearchView from './Views/SearchView';

export default class App extends Component {
  render() {
    return (
      <div className="App" >
        <SearchView/>
      </div>
    );
  }
}