# IT2810 - Project 4 - Group 48

## The site

In this project we decided to create a pokedex as it would be a decent but not too large data-set. In this system you can search up pokemon and display information about them. You can search on name and pokedex number, as well as filter based on type when searching by name (you can only get one result back when it is a number anyway).

There are some pokemon in our data-set that does not follow the standard name format and as such will not load their portraits properly.

Due to time constraints we did not manage to fully implement the storage of user generated data in our system. The back-end does have support for it however.

## Setup

```bash
git clone https://gitlab.stud.idi.ntnu.no/it2810-h18/prosjekt3/gruppe48
cd gruppe48
npm install
npm start
```

---

### Front-end

#### Components

We made a handful of components to help with displaying information ourselves, they are all placed in the `src/Components` folder. `PokePortrait` will create an image-like component with a background and border based on the typing of the pokemon. `PokeType` mimics the type tag like from the games. `PokeStats` is a radar chart that displays the stats of the pokemon. This component makes use of react-vis. `SearchItem` is the component displayed in the list when a search result is shown. The `typeColors` file is not technically a component (it functions more like a enum-map) but is used by several of the components and is therefore placed in the same folder.

Since the results where handed to the components in json form they did not have any reason to keep state, as such data is passed inn as properties. Component simply display data from those properties or slightly modify them before displaying.

#### Views

We have two views in this application, `ItemView` and `SearchView`. `SearchView` is the main page where one can search for pokemon and have them displayed in a list. `ItemView` is used when clicking on an element in the list to view more details of the clicked pokemon.

#### mobx

Mobx is a state management framework designed to be non-intrusive in existing codebase. As such it is simple to use and adds little overhead. To make use of it you simply provide a mapping between the variables/functions and their role (observer, observable). It normal to use decorators to do this, but we encountered some issues when enabling decorators in this project and decided we would not spend time to fix this. Mobx can be used perfectly fine without decorators by using the `decorate()` function instead.

Mobx is really convenient as one can simply just give a component a variable as a prop and it will automatically update when the variable is changed, no need for callback functions! We chose to use mobx over redux as it is a lot simpler to integrate due to it's non intrusiveness and seemed fit a small prototype-app like this better than redux.

#### react-modal

React modal is a really simple way to get a modal(popover) view in a react app. There is not that much to say about this, you simply define the methods for what to do when it closes and opens and put the content inside the tag.

#### react-vis

React-vis is a set of visualization components. It can be used to draw graphs and display data in a variety of ways. In this project we used it to create the radar chart that displays the stats of the pokemon. We decided to use this because we where after a specific type of chart and they provided a simple to use implementation of it.

#### react-search-field

This is another simple component that functions like a standard search bar, button included. The way you use this is pretty much the same way you would use a button + an input field, they are just combined into one component.

---

### Back-end

#### Express

Express is a minimal and flexible Node.js web framework. In our project it is used for hosting our back-end logic. It’s a popular framework and because of that It’s simple to find both documentation and complementary APIs. We used this framework because it was both obligatory and actually quite simple to use.

To handle CORS we used a CORS middleware for Express.

Express and the CORS middleware can be found below.

https://expressjs.com/

https://github.com/expressjs/cors

#### GraphQL

We were required to use a REST-style API for our project and the choice fell on GraphQL. While different from traditional REST APIs it’s very flexible. GraphQL is both a query language, and a runtime to run those queries. It’s a very popular alternative to traditional REST, seemed both simple and powerful, and we were curious to try it out, which is why we decided to use it.

In our project GraphQl is used to handle requests from the front-end and to model and structure our back-end. Using it went well and since almost all our code was javascript it integrated pretty well considering pretty much all the data is Javascript Objects. The built in development tools were quite handy to use in both debugging and testing, GraphiQL was especially handy as we could try out queries there beforehand.

Since we were using Express, we used GraphQL HTTP Server Middleware made especially for the Express framework, this made working with both easier.

GraphQl and GraphQL HTTP Server Middleware can be found below.

https://graphql.github.io/

https://github.com/graphql/express-graphql

#### SQL

GraphQL provides a way for the front-end to speak to our back-end and also model our data in our back end, but provided no way to store it persistently across sessions and downtime. To save and contain our data in a persistent way, we decided to set up a database with MySQL. This was technology we were somewhat familiar with from before and that is why we used it. We decided to use NTNUs MySQL server to host our database.

To interface with MySQL and connect with our database we used Sequelize. Sequelize is a Node.js API used for connecting to an SQL database. We used it because it had good documentation and seemed simple to use. It has fancy modelling capabilities and such, but we ended up using raw SQL queries instead.

NTNUs MySQL and Sequelize can be found below.

http://docs.sequelizejs.com/

https://innsida.ntnu.no/wiki/-/wiki/English/Using+MySQL+at+NTNU

For our data about pokemon we used the CSV files from the link below and imported them into our database.

https://github.com/veekun/pokedex

---

### Testing

All members of this group where rather busy during the time of this project, therefore we did not have time to complete everything in this assignment. We prioritized to create a functioning system over writing simple unit tests. Sadly we did not have time to integrate end to end testing as well.