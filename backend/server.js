var express = require('express');
var graphqlHTTP = require('express-graphql');
var { buildSchema } = require('graphql');
const Sequelize = require('sequelize');

//Connect to database
var db = new Sequelize(
  'johngu_pokedata','johngu_db', 'dbkk12',
  {
    host    : 'mysql.stud.ntnu.no',
    port    :  3306,
    dialect : 'mysql'
  }
);
//check if database connection has been established
db.authenticate().then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
});


// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
    type Pokemon {
        pokedex_number : Int
        classification : String
        name : String
        height_m : Int
        weight_kg : Int

        type1 : String
        type2 : String
        ability1 : String
        ability2 : String
        ability3 : String

        generation : Int

        hp : Int
        attack : Int
        defense : Int
        sp_attack : Int
        sp_defense : Int
        speed : Int
    }

    type Comment {
        pokemon_id : Int
        text : String
    }
    
    type Query {
        findPokemon(name : String, type : String, sortBy : String, order : String) : [Pokemon]
        getPokemon(dexNumber : Int!) : [Pokemon]
        getComments(dexNumber : Int!) : [Comment]
    }

    type Mutation {
        addComment(dexNumber : Int!, text : String!) : Comment
    }
`);

// The root provides a resolver function for each API endpoint
var root = {
    //gets the pokemon with the specified dexnumber
    getPokemon: ( {dexNumber}) => {
        var query = `
            SELECT pokemon.species_id AS pokedex_number, pokemon.identifier AS name, pokemon.height AS height_m, pokemon.weight AS weight_kg,(
                    SELECT types.identifier
                    FROM pokemon_types
                    INNER JOIN types ON pokemon_types.type_id = types.id
                    WHERE pokemon_types.pokemon_id = pokemon.id AND pokemon_types.slot = 1
            ) AS type1, (
                    SELECT types.identifier
                    FROM pokemon_types
                    INNER JOIN types ON pokemon_types.type_id = types.id
                    WHERE pokemon_types.pokemon_id = pokemon.id AND pokemon_types.slot = 2
            ) AS type2, (
                    SELECT pokemon_stats.base_stat
                    FROM pokemon_stats
                    WHERE pokemon_stats.pokemon_id = pokemon.id AND pokemon_stats.stat_id = 1
            ) AS hp , (
                    SELECT pokemon_stats.base_stat
                    FROM pokemon_stats
                    WHERE pokemon_stats.pokemon_id = pokemon.id AND pokemon_stats.stat_id = 2
            ) AS attack , (
                    SELECT pokemon_stats.base_stat
                    FROM pokemon_stats
                    WHERE pokemon_stats.pokemon_id = pokemon.id AND pokemon_stats.stat_id = 3
            ) AS defense , (
                    SELECT pokemon_stats.base_stat
                    FROM pokemon_stats
                    WHERE pokemon_stats.pokemon_id = pokemon.id AND pokemon_stats.stat_id = 4
            ) AS sp_attack, (
                    SELECT pokemon_stats.base_stat
                    FROM pokemon_stats
                    WHERE pokemon_stats.pokemon_id = pokemon.id AND pokemon_stats.stat_id = 5
            ) AS sp_defense , (
                    SELECT pokemon_stats.base_stat
                    FROM pokemon_stats
                    WHERE pokemon_stats.pokemon_id = pokemon.id AND pokemon_stats.stat_id = 6
            ) AS speed, (
                    SELECT pokemon_species.generation_id
                    FROM pokemon_species
                    WHERE pokemon_species.id = pokemon.id
            ) AS generation, (
                    SELECT abilities.identifier
                    FROM pokemon_abilities
                    INNER JOIN abilities ON pokemon_abilities.ability_id = abilities.id
                    WHERE pokemon_abilities.pokemon_id = pokemon.id AND pokemon_abilities.slot = 1
            ) AS ability1, (
                    SELECT abilities.identifier
                    FROM pokemon_abilities
                    INNER JOIN abilities ON pokemon_abilities.ability_id = abilities.id
                    WHERE pokemon_abilities.pokemon_id = pokemon.id AND pokemon_abilities.slot = 2
            ) AS ability2, (
                    SELECT abilities.identifier
                    FROM pokemon_abilities
                    INNER JOIN abilities ON pokemon_abilities.ability_id = abilities.id
                    WHERE pokemon_abilities.pokemon_id = pokemon.id AND pokemon_abilities.slot = 3
            ) AS ability3, (
                    SELECT pokemon_species_names.genus
                    FROM pokemon_species_names
                    WHERE pokemon_species_names.pokemon_species_id = pokemon.id
            ) AS classification
            FROM pokemon
            WHERE pokemon.id = ?`;
        
        return db.query(query, { raw:true, replacements: [dexNumber], type: db.QueryTypes.SELECT });
    },
    //Finds pokemon with the 'name' string within its name, the type and sorts it according to the sortBy parameter. The sortBy parameter can be any of the Pokemon type's attributes. order is either 'asc' or 'desc'
    findPokemon: ( {name, type, sortBy, order}) => {
        
        if (name == undefined) {
            name = "";
        }
        
        var query = `
            SELECT pokemon.species_id AS pokedex_number, pokemon.identifier AS name, pokemon.height AS height_m, pokemon.weight AS weight_kg,(
                    SELECT types.identifier
                    FROM pokemon_types
                    INNER JOIN types ON pokemon_types.type_id = types.id
                    WHERE pokemon_types.pokemon_id = pokemon.id AND pokemon_types.slot = 1
            ) AS type1, (
                    SELECT types.identifier
                    FROM pokemon_types
                    INNER JOIN types ON pokemon_types.type_id = types.id
                    WHERE pokemon_types.pokemon_id = pokemon.id AND pokemon_types.slot = 2
            ) AS type2, (
                    SELECT pokemon_stats.base_stat
                    FROM pokemon_stats
                    WHERE pokemon_stats.pokemon_id = pokemon.id AND pokemon_stats.stat_id = 1
            ) AS hp , (
                    SELECT pokemon_stats.base_stat
                    FROM pokemon_stats
                    WHERE pokemon_stats.pokemon_id = pokemon.id AND pokemon_stats.stat_id = 2
            ) AS attack , (
                    SELECT pokemon_stats.base_stat
                    FROM pokemon_stats
                    WHERE pokemon_stats.pokemon_id = pokemon.id AND pokemon_stats.stat_id = 3
            ) AS defense , (
                    SELECT pokemon_stats.base_stat
                    FROM pokemon_stats
                    WHERE pokemon_stats.pokemon_id = pokemon.id AND pokemon_stats.stat_id = 4
            ) AS sp_attack, (
                    SELECT pokemon_stats.base_stat
                    FROM pokemon_stats
                    WHERE pokemon_stats.pokemon_id = pokemon.id AND pokemon_stats.stat_id = 5
            ) AS sp_defense , (
                    SELECT pokemon_stats.base_stat
                    FROM pokemon_stats
                    WHERE pokemon_stats.pokemon_id = pokemon.id AND pokemon_stats.stat_id = 6
            ) AS speed, (
                    SELECT pokemon_species.generation_id
                    FROM pokemon_species
                    WHERE pokemon_species.id = pokemon.id
            ) AS generation, (
                    SELECT abilities.identifier
                    FROM pokemon_abilities
                    INNER JOIN abilities ON pokemon_abilities.ability_id = abilities.id
                    WHERE pokemon_abilities.pokemon_id = pokemon.id AND pokemon_abilities.slot = 1
            ) AS ability1, (
                    SELECT abilities.identifier
                    FROM pokemon_abilities
                    INNER JOIN abilities ON pokemon_abilities.ability_id = abilities.id
                    WHERE pokemon_abilities.pokemon_id = pokemon.id AND pokemon_abilities.slot = 2
            ) AS ability2, (
                    SELECT abilities.identifier
                    FROM pokemon_abilities
                    INNER JOIN abilities ON pokemon_abilities.ability_id = abilities.id
                    WHERE pokemon_abilities.pokemon_id = pokemon.id AND pokemon_abilities.slot = 3
            ) AS ability3, (
                    SELECT pokemon_species_names.genus
                    FROM pokemon_species_names
                    WHERE pokemon_species_names.pokemon_species_id = pokemon.id
            ) AS classification
            FROM pokemon
            WHERE pokemon.identifier LIKE '%` + name + "%' ";
        
        if (type != null) {
            query += 'HAVING type1 = "' + type + '" or type2 = "' + type + '" ';
        }
        if (sortBy != undefined) {
            query += 'ORDER BY ' + sortBy;
            if (order != undefined) {
                query += ' ' + order;
            }
        }
        
        return db.query(query, { raw:true,  type: db.QueryTypes.SELECT });
    },
    //Adds a comment to a pokemon
    addComment: ( {dexNumber, text}) => {
        var query = `
            INSERT INTO pokemon_comment (pokemon_comment.pokemon_id, pokemon_comment.text)
            VALUES (?, ?);`;
        
        db.query(query, { raw:true, replacements: [dexNumber, text], type: db.QueryTypes.SELECT });
        
        return {pokemon_id:dexNumber, text:text};
    },
    //Gets the comments of a pokemon
    getComments: ( {dexNumber}) => {
        var query = `
            SELECT pokemon_comment.pokemon_id, pokemon_comment.text
            FROM pokemon_comment
            WHERE pokemon_comment.pokemon_id = 3`;
                
        return db.query(query, { raw:true, replacements: [dexNumber], type: db.QueryTypes.SELECT });
    },
};


var app = express();

//Adds logging to the server, see when someone connects.
function loggingMiddleware(req, res, next) {
  console.log('ip:', req.ip);
  next();
}

//Adds CORS to the server.
var cors = require('cors');
app.use(cors({origin: '*'}))

app.use(loggingMiddleware);
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));
app.listen(4000);
console.log('Running a GraphQL API server at localhost:4000/graphql');